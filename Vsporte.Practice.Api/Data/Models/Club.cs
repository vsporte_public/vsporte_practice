﻿using System;
using System.Collections.Generic;

namespace Vsporte.Practice.Api.Data.Models;

public partial class Club
{
    public Guid ClubId { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<ClubPlayer> ClubPlayers { get; } = new List<ClubPlayer>();
}

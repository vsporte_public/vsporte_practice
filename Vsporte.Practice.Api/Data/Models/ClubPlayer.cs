﻿using System;
using System.Collections.Generic;

namespace Vsporte.Practice.Api.Data.Models;

public partial class ClubPlayer
{
    public Guid ClubId { get; set; }

    public Guid PlayerId { get; set; }

    public string? Comment { get; set; }

    public virtual Club Club { get; set; } = null!;

    public virtual Player Player { get; set; } = null!;
}

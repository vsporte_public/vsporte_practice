﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Vsporte.Practice.Api.Data.Models;

public partial class DataContext : DbContext
{
    public DataContext()
    {
    }

    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Club> Clubs { get; set; }

    public virtual DbSet<ClubPlayer> ClubPlayers { get; set; }

    public virtual DbSet<Player> Players { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=practice;Username=practice_user;Password=1234qwer");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Club>(entity =>
        {
            entity.HasKey(e => e.ClubId).HasName("Club_pk");

            entity.ToTable("Club");

            entity.Property(e => e.ClubId)
                .ValueGeneratedNever()
                .HasColumnName("club_id");
            entity.Property(e => e.Name).HasColumnName("name");
        });

        modelBuilder.Entity<ClubPlayer>(entity =>
        {
            entity.HasKey(e => new { e.ClubId, e.PlayerId }).HasName("ClubPlayer_pk");

            entity.ToTable("ClubPlayer");

            entity.Property(e => e.ClubId).HasColumnName("club_id");
            entity.Property(e => e.PlayerId).HasColumnName("player_id");
            entity.Property(e => e.Comment).HasColumnName("comment");

            entity.HasOne(d => d.Club).WithMany(p => p.ClubPlayers)
                .HasForeignKey(d => d.ClubId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("ClubPlayer_Club_club_id_fk");

            entity.HasOne(d => d.Player).WithMany(p => p.ClubPlayers)
                .HasForeignKey(d => d.PlayerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("ClubPlayer_Player_player_id_fk");
        });

        modelBuilder.Entity<Player>(entity =>
        {
            entity.HasKey(e => e.PlayerId).HasName("Player_pk");

            entity.ToTable("Player");

            entity.Property(e => e.PlayerId)
                .ValueGeneratedNever()
                .HasColumnName("player_id");
            entity.Property(e => e.Name).HasColumnName("name");
            entity.Property(e => e.Number).HasColumnName("number");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

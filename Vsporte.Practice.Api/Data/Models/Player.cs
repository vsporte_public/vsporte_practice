﻿using System;
using System.Collections.Generic;

namespace Vsporte.Practice.Api.Data.Models;

public partial class Player
{
    public Guid PlayerId { get; set; }

    public string Name { get; set; } = null!;

    public int Number { get; set; }

    public virtual ICollection<ClubPlayer> ClubPlayers { get; } = new List<ClubPlayer>();
}

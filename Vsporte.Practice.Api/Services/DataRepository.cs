﻿using Microsoft.EntityFrameworkCore;
using Vsporte.Practice.Api.Data.Models;

namespace Vsporte.Practice.Api.Services;

public class DataRepository
{
    public DataRepository()
    {
    }

    public Player[] GetAllPlayers()
    {
        var dataContext = new DataContext();
        return dataContext.Players.ToArray();
    }

    public async Task<bool> IsClubHaveNotPlayers(Guid clubId)
    {
        var dataContext = new DataContext();
        var club = await dataContext.Clubs.FirstAsync(x => x.ClubId == clubId);
        if (club.ClubPlayers.Any()) return false;
        return true;
    }

    public async Task DeletePlayer(Guid playerId, CancellationToken cancellationToken)
    {
        var dataContext = new DataContext();
        var player = await dataContext.Players.FirstAsync(x => x.PlayerId == playerId, cancellationToken);
        dataContext.Players.Remove(player);
    }
    
    public async Task LinkPlayerToClub(Guid playerId, Guid clubId, CancellationToken cancellationToken)
    {
        var dataContext = new DataContext();
        var player = await dataContext.Players.FirstAsync(x => x.PlayerId == playerId, cancellationToken);
        var club = await dataContext.Clubs.FirstAsync(x => x.ClubId == clubId, cancellationToken);
        var clubPlayer = new ClubPlayer
        {
            Player = player,
            Club = club
        };
        player.ClubPlayers.Add(clubPlayer);
    }
}
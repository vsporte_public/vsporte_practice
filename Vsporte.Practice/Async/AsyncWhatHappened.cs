﻿namespace Vsporte.Practice.Async;

public class AsyncWhatHappened
{
    private static string _result = "Init";

    private static async Task<string> SaySomething() {
        _result = "Hello";
        await Task.Delay(5);
        _result = "Hello world!";
        return "Answer";
    }

    public static void Process() {
        SaySomething();
        Console.WriteLine(_result);
    }
}
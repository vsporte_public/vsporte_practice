﻿namespace Vsporte.Practice.Async;

public class CatcherWhatHappened
{
    private static Task<int> Foo()
    {
        try
        {
            return Bar();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return Task.FromResult(0);
    }
    
    private static async Task<int> Bar()
    {
        var value = int.MaxValue;
        return value + 1;
    }

    public static async Task Process()
    {
        var foo = await Foo();
        Console.WriteLine(foo);
    }
}
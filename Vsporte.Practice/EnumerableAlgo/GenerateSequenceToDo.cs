﻿using System.Collections;

namespace Vsporte.Practice.EnumerableAlgo;

public class GenerateSequenceToDo
{
    private static void Print(int basement, int count)
    {
        // foreach (var i in new GenerateSequenceToDo(basement, count))
        // {
            // Console.WriteLine(i);
        // }
        // Console.WriteLine();
    }
    public static void Process()
    {
        Print(3, 4);
        // 3
        // 6
        // 9
        // 12 
        
        Print(0, 2);
        // 0
        // 0
        
        Print(-4, 3);
        // -4
        // -8
        // -12
        
        Print(0, -1);
        // Exception
    }
}
﻿namespace Vsporte.Practice.Pattern;

public class Singleton
{
    private Singleton()
    {
    }

    public static Singleton Instance = new();
}
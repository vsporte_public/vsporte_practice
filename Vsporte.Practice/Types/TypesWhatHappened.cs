﻿namespace Vsporte.Practice.Types;

public class TypesWhatHappened
{
    public static readonly float NumberBar = 5;
    private static readonly int NumberFoo = 5;
    private string _location;
    private static DateTime _time;
 
    public void Process()
    {
        Console.WriteLine(_location);
        Console.WriteLine(_time);

        // _location ??= "Hello";
        // _time ??= DateTime.Now;
        
        Console.WriteLine(_location);
        Console.WriteLine(_time);
        
        Console.WriteLine(NumberFoo == NumberBar);
        Console.WriteLine(NumberFoo.Equals(NumberBar));
    }

    public void EditRef(ref int a)
    {
        a = 4;
    }
    public void EditOut(out int a)
    {
        a = 4;
    }

    public int Field;
    public int Property { get; set; }
}